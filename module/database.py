import pymysql

class Database:
    def connect(self):
        return pymysql.connect("localhost","root","","sekolah" )

#crud siswa
    def read_siswa(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            if id == None:
                cursor.execute("SELECT * FROM siswa order by kelas asc")
            else:
                cursor.execute("SELECT * FROM siswa where id_siswa = %s order by kelas asc", (id,))

            return cursor.fetchall()
        except:
            return ()
        finally:
            con.close()

    def insert_siswa(self,data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("INSERT INTO siswa(nisn,nama,jk,kelas,ttl,alamat) VALUES(%s, %s, %s, %s, %s, %s)", (data['nisn'],data['nama'],data['jk'],data['kelas'],data['ttl'],data['alamat'],))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def update_siswa(self, id, data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("UPDATE siswa set nisn = %s, nama = %s, jk = %s, kelas = %s, ttl = %s, alamat = %s where id_siswa = %s", (data['nisn'],data['nama'],data['jk'],data['kelas'],data['ttl'],data['alamat'],id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def delete_siswa(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("DELETE FROM siswa where id_siswa = %s", (id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

#crud guru
    def read_guru(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            if id == None:
                cursor.execute("SELECT * FROM guru")
            else:
                cursor.execute("SELECT * FROM guru where id_guru = %s", (id,))

            return cursor.fetchall()
        except:
            return ()
        finally:
            con.close()

    def insert_guru(self,data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("INSERT INTO guru(nip,nama,jk,ttl,alamat) VALUES(%s, %s, %s,%s,%s)", (data['nip'],data['nama'],data['jk'],data['ttl'],data['alamat'],))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def update_guru(self, id, data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("UPDATE guru set nip = %s, nama = %s, jk = %s, ttl = %s, alamat = %s where id_guru = %s", (data['nip'],data['nama'],data['jk'],data['ttl'],data['alamat'],id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def delete_guru(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("DELETE FROM guru where id_guru = %s", (id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()
    
#crud mapel
    def read_mapel(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            if id == None:
                cursor.execute("SELECT * FROM mapel")
            else:
                cursor.execute("SELECT * FROM mapel where id_mapel = %s", (id,))

            return cursor.fetchall()
        except:
            return ()
        finally:
            con.close()

    def insert_mapel(self,data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("INSERT INTO mapel(nama_mapel,id_guru) VALUES(%s, %s)", (data['nama_mapel'],data['id_guru'],))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def update_mapel(self, id, data):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("UPDATE mapel set nama_mapel = %s, id_guru = %s where id_mapel = %s", (data['nama_mapel'],data['id_guru'],id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    def delete_mapel(self, id):
        con = Database.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("DELETE FROM mapel where id_mapel = %s", (id,))
            con.commit()

            return True
        except:
            con.rollback()

            return False
        finally:
            con.close()

    # def login():
    #     # Check if account exists using MySQL
    #     con = Database.connect(self)
    #     cursor = con.cursor()
    #     cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password))
    #     # Fetch one record and return result
    #     account = cursor.fetchone()

    