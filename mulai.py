from flask import Flask, flash, render_template, redirect, url_for, request, session
from module.database import Database
from flask_mysqldb import MySQL,MySQLdb
import bcrypt


app = Flask(__name__)
app.secret_key = "myk3y"
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'sekolah'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)
db = Database()

# @app.route('/')
# def listbarang():
#     return render_template('listbarang.html')

@app.route('/')
def index():
    data = db.read_siswa(None)
    return render_template('index.html', datasiswa = data)

#     data = db.read(None)
#     return render_template('index.html', data = data)

@app.route('/datasiswa')
def datasiswa():
    data = db.read_siswa(None)
    return render_template('datasiswa.html', datasiswa = data)

@app.route('/tambahsiswa/')
def tambahsiswa():
    return render_template('tambahsiswa.html')

@app.route('/tambahdatasiswa', methods = ['POST', 'GET'])
def tambahdatasiswa():
    if request.method == 'POST' and request.form['save']:
        if db.insert_siswa(request.form):
            flash("Siswa baru telah ditambahkan")
        else:
            flash("Siswa tidak bisa ditambahkan")

        return redirect(url_for('datasiswa'))
    else:
        return redirect(url_for('datasiswa'))

@app.route('/updatesiswa/<int:id>/')
def updatesiswa(id):
    data = db.read_siswa(id);

    if len(data) == 0:
        return redirect(url_for('datasiswa'))
    else:
        session['update'] = id
        return render_template('updatesiswa.html', datasiswa = data)

@app.route('/updatesiswaaksi', methods = ['POST'])
def updatesiswaaksi():
    if request.method == 'POST' and request.form['update']:

        if db.update_siswa(session['update'], request.form):
            flash('Data Siswa telah diupdate')

        else:
            flash('Data siswa tidak bisa diupdate')

        session.pop('update', None)

        return redirect(url_for('datasiswa'))
    else:
        return redirect(url_for('datasiswa'))

@app.route('/haldeletesiswa/<int:id>/')
def haldeletesiswa(id):
    data = db.read_siswa(id);

    if len(data) == 0:
        return redirect(url_for('datasiswa'))
    else:
        session['delete'] = id
        return render_template('deletesiswa.html', datasiswa = data)

@app.route('/deletesiswa', methods = ['POST'])
def deletesiswa():
    if request.method == 'POST' and request.form['delete']:

        if db.delete_siswa(session['delete']):
            flash('Siswa telah dihapus')

        else:
            flash('Siswa tidak bisa dihapus')

        session.pop('delete', None)

        return redirect(url_for('datasiswa'))
    else:
        return redirect(url_for('datasiswa'))

# @app.route('/login',methods=["GET","POST"])
# def login():
#     if request.method == 'POST':
#         email = request.form['email']
#         password = request.form['password'].encode('utf-8')

#         curl = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
#         curl.execute("SELECT * FROM users WHERE email=%s",(email,))
#         user = curl.fetchone()
#         curl.close()

#         if len(user) > 0:
#             if bcrypt.hashpw(password, user["password"].encode('utf-8')) == user["password"].encode('utf-8'):
#                 session['username'] = user['username']
#                 session['email'] = user['email']
#                 return redirect(url_for('index'))
#             else:
#                 return "Error password and email not match"
#         else:
#             return "Error user not found"
#     else:
#         return render_template("login.html")

# @app.route('/logout/', methods=["GET", "POST"])
# def logout():
#     session.clear()
#     return render_template("index.html")

# @app.route('/register', methods=["GET", "POST"])
# def register():
#     if request.method == 'GET':
#         return render_template("register.html")
#     else:
#         username = request.form['username']
#         email = request.form['email']
#         password = request.form['password'].encode('utf-8')
#         hash_password = bcrypt.hashpw(password, bcrypt.gensalt())

#         cur = mysql.connection.cursor()
#         cur.execute("INSERT INTO users (username, email, password) VALUES (%s,%s,%s)",(username,email,hash_password,))
#         mysql.connection.commit()
#         session['username'] = request.form['username']
#         session['email'] = request.form['email']
#         return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(error):
    return render_template('error.html')

if __name__ == '__main__':
    app.debug = True
    app.run(port=8080, host="127.2.2.2")
